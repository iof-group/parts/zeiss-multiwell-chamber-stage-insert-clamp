# Multiwell chambered slide holder for Zeiss stage insert

## Project structure

```bash
This project
    ├── assets # create new folders if needed
    │   ├── drawings # technical drawings
    │   └── images # pictures or screenshots of the parts
    └── src # actual .stl source files
```

## One sentence pitch
![](assets/images/Sample-carrier-holder-disassembled.jpeg)

Fasten multiwell chambered glass slides of various dimensions onto the (ISTA custom-made) slide stage insert for Zeiss LSM microscope stages.

## How it works
A sample carrier has to be fixed to the stage insert of the microscope for all types of microscopes (upright, inverted, vertical). Chambered glass slides come in various dimensions, which makes it necessary to adjust its holder. An object was designed to fasten sample carriers of various lengths onto the stage insert using magnets.
The dimensions of the object are chosen to be fitting tightly to the plastic chambers on the slide, so that no movement in X or Y is possible when the sample carrier is mounted in the holder. The magnets fixed position additionally allow to easily remove the sample carrier, switch the objective, apply immersion medium and mount the sample again, with very minimal X/Y stage coordinate adjustment needed.

This is how it looks on a microscope stage:

![](assets/images/Sample-carrier-holder-on-stage.jpeg)

## Authors
Maximilian Schuster, Imaging and Optics Facility (IOF), IST Austria, iof@ista.ac.at.

## Acknowledgment
Robert Hauschild for printing the object.